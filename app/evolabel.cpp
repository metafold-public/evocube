#include <igl/readOBJ.h>
#include <igl/writeOBJ.h>
#include <igl/writePLY.h>
#include <igl/read_triangle_mesh.h>
#include <igl/file_dialog_save.h>
#include <ctime>
#include <random>
#include <queue>
#include <nlohmann/json.hpp>

#include "graphcut_labeling.h"
#include "flagging_utils.h"
#include "logging.h"
#include "chart.h"
#include "quick_label_ev.h"
#include "evocube.h"
#include "evaluator.h"
#include "labeling_individual.h"
#include "archive.h"

int main(int argc, char *argv[]){
    std::string input_tris = "../data/bunny/boundary.obj";
    if (argc > 1) input_tris = argv[1];

    //std::srand(std::time(nullptr));
    std::srand(1);

    auto time_before_init = std::chrono::steady_clock::now();

    Eigen::MatrixXd V;
    Eigen::MatrixXi F;
    igl::read_triangle_mesh(input_tris, V, F);

    std::shared_ptr<Evocube> evo = std::make_shared<Evocube>(Evocube(V, F));
    std::shared_ptr<const QuickLabelEv> qle = std::make_shared<const QuickLabelEv>(QuickLabelEv(V, F));
    const Evaluator evaluator(evo, qle); 

    // ---- GENERATE INITIAL SOLUTION ---- //

    int compact_coeff = 1;
    int fidelity_coeff = 3;
    Eigen::VectorXi labeling_init = graphcutFlagging(V, F, evo->N_, evo->TT_, compact_coeff, fidelity_coeff);
    std::shared_ptr<LabelingIndividual> ancestor = std::make_shared<LabelingIndividual>(LabelingIndividual(evo, qle, labeling_init));

    ancestor->updateChartsAndTPs();
    ancestor->repairHighValenceCorner();
    ancestor->updateChartsAndTPs();
    ancestor->repairOppositeLabels();
    ancestor->updateChartsAndTPs();

    ancestor->repairUnspikeLabeling();
    ancestor->updateChartsAndTPs(true);
    ancestor->updateTimestamps();
    
    double current_score = evaluator.evaluate(*ancestor);
    Eigen::MatrixXd def_V;// = qle->computeDeformedV(ancestor.getLabeling());

    Archive<std::shared_ptr<LabelingIndividual>> archive(10);
    archive.insert(ancestor, current_score);

    auto pickMutation = [](double progress_percentage){
        double r = static_cast<double>(std::rand() % 1000)/1000.0;
        double prob0 = 0.5 - 0.2 * progress_percentage;
        double prob1 = 0.0 + 0.2 * progress_percentage;
        double prob2 = 0.3 - 0.0 * progress_percentage;
        double prob3 = 0.2 - 0.0 * progress_percentage;
        if (r < prob0) return 0;
        if (r < prob0 + prob1) return 1;
        if (r < prob0 + prob1 + prob2) return 2;
        else return 3;
    };

    double sum_time_create_indiv = 0;
    double sum_time_cross = 0;
    double sum_time_mut = 0;
    double sum_time_chart = 0;
    double sum_time_archive = 0;
    double sum_time_eval = 0;


    // ---- GENETIC OPTIMIZATION ---- //

    auto time_before_evocube = std::chrono::steady_clock::now();

    int n_generations = 40;
    int max_mut = 100;

    int convergence_stop = 3; // If score doesn't improve for convergence_stop consecutive iterations, stop
    int convergence_count = 0;
    double previous_best_score = -1; 

    for (int generation=0; generation<n_generations; generation++){

        evo->timestamp_ ++;

        Archive<std::shared_ptr<LabelingIndividual>> gen_archive(5);

        std::vector<std::shared_ptr<LabelingIndividual>> new_gen;
        std::vector<double> new_scores;

        new_gen.resize(max_mut);
        new_scores.resize(max_mut);

        if (archive.bestScore() != previous_best_score){
            convergence_count = 0;
            previous_best_score = archive.bestScore(); 
        }
        else {
            convergence_count ++;
            if (convergence_count > convergence_stop){
                coloredPrint("Reached convergence, stopping labeling optimization", "green");
                n_generations = generation;
                break;
            }
        }

        // -- Generate new individuals through MUTATIONS -- //
        #pragma omp parallel for
        for (int i=0; i<max_mut; i++){
            std::cout << "Generation " << generation << ", Mutation: " << i << std::endl;

            auto time_new_indiv = std::chrono::steady_clock::now();    
            int pick = archive.probabilisticIndividual();
            std::shared_ptr<LabelingIndividual> new_indiv = std::make_shared<LabelingIndividual>(*archive.getIndiv(pick));
            new_indiv->updateChartsAndTPs(true); // OPTIM: could be skipped if we kept info from parent

            auto time_before_mutation = std::chrono::steady_clock::now();     

            int mutation_type = pickMutation(static_cast<double>(generation)/n_generations);
            if (mutation_type == 0) new_indiv->mutationVertexGrow();
            if (mutation_type == 1) new_indiv->mutationRemoveChart();
            if (mutation_type == 2) new_indiv->mutationGreedyPath();
            if (mutation_type == 3) new_indiv->mutationBorderGrow();

            auto time_after_mutation = std::chrono::steady_clock::now();

            new_indiv->updateChartsAndTPs();
            new_indiv->repairUnspikeLabeling();
            new_indiv->updateChartsAndTPs(true);
            new_indiv->updateTimestamps();

            auto time_after_charts = std::chrono::steady_clock::now();
            
            double new_score = evaluator.evaluate(*new_indiv);
            new_gen[i] = new_indiv;
            new_scores[i] = new_score;

            auto time_after_eval = std::chrono::steady_clock::now();

            double time_create_indiv = measureTime(time_new_indiv, time_before_mutation);
            double time_mut = measureTime(time_before_mutation, time_after_mutation);
            double time_chart = measureTime(time_after_mutation, time_after_charts);
            double time_eval = measureTime(time_after_charts, time_after_eval);
            double totaltime = measureTime(time_new_indiv, time_after_eval);

            sum_time_create_indiv += time_create_indiv;
            sum_time_mut += time_mut;
            sum_time_chart += time_chart;
            sum_time_eval += time_eval;

            #ifdef PRINT_EVOCUBE_TIMINGS
            std::cout << "Individual mutation, time repartition:" << std::endl;
            std::cout << "\tCreation \t" << time_create_indiv << std::endl;
            std::cout << "\tMutation \t" << time_mut << std::endl;
            std::cout << "\tCharts   \t" << time_chart << std::endl;
            std::cout << "\tEval.    \t" << time_eval << std::endl;
            std::cout << "\tTotal:   \t" << totaltime << std::endl;
            #endif
        }

        // Insert new indivs into archive (not done in the loop because it's not //)
        for (int i=0; i<new_gen.size(); i++){
            gen_archive.insert(new_gen[i], new_scores[i]);
        }

        auto time_after_mutations = std::chrono::steady_clock::now();

        // -- Generate new individuals through CROSSING -- //
        int n_cross = 10;
        for (int i=0; i<n_cross; i++){
            std::cout << "Generation " << generation << ", Crossing: " << i << std::endl;

            auto time_before_cross = std::chrono::steady_clock::now();    
            int p1 = gen_archive.probabilisticIndividual();
            int p2 = gen_archive.probabilisticIndividual();
            if (p1 == p2) continue;
            std::shared_ptr<LabelingIndividual> child = std::make_shared<LabelingIndividual>(
                                                                LabelingIndividual(*gen_archive.getIndiv(p1), 
                                                                                   *gen_archive.getIndiv(p2)));

            auto time_after_cross = std::chrono::steady_clock::now();    

            child->updateChartsAndTPs();
            child->repairUnspikeLabeling();
            child->updateChartsAndTPs(true); // TPs not needed?
            child->updateTimestamps(); // not needed?

            auto time_after_charts = std::chrono::steady_clock::now();   

            double new_score = evaluator.evaluate(*child);
            auto time_after_eval = std::chrono::steady_clock::now();   

            gen_archive.insert(child, new_score);
            auto time_after_archive = std::chrono::steady_clock::now();   


            sum_time_cross += measureTime(time_before_cross, time_after_cross);
            sum_time_chart += measureTime(time_after_cross, time_after_charts);
            sum_time_eval += measureTime(time_after_charts, time_after_eval);
            sum_time_archive += measureTime(time_after_eval, time_after_archive);
        }

        auto time_before_archive = std::chrono::steady_clock::now();
        // Insert generation into general archive
        for (int id=0; id<gen_archive.getSize(); id++){
            archive.insert(gen_archive.getIndiv(id), gen_archive.getScore(id));
        }
        auto time_after_archive = std::chrono::steady_clock::now();
        sum_time_archive += measureTime(time_before_archive, time_after_archive);
        //std::cout << "End of generation, additional time:" << std::endl;
        //std::cout << "\tGeneral archive \t" << time_insert_archive << std::endl;
    }

    auto time_after_evocube = std::chrono::steady_clock::now();


    // ---- FINAL SOLUTION ---- //
    std::shared_ptr<LabelingIndividual> final_indiv = archive.getIndiv(0);

    final_indiv->updateChartsAndTPs();
    final_indiv->removeChartsWithTooFewNeighbors();
    final_indiv->updateChartsAndTPs();
    final_indiv->repairHighValenceCorner();
    final_indiv->updateChartsAndTPs();
    final_indiv->repairOppositeLabels();
    final_indiv->updateChartsAndTPs();
    final_indiv->updateChartsAndTPs(true);
    def_V = qle->computeDeformedV(final_indiv->getLabeling());

    coloredPrint("Final validity: " + std::to_string(final_indiv->invalidityScore()), "cyan");

    Eigen::MatrixXd threshold_colors = qle->distoAboveThreshold(final_indiv->getLabeling(), 100.0);

    auto time_after_post = std::chrono::steady_clock::now();
    double time_init_evo = measureTime(time_before_init, time_before_evocube);
    double time_evocube = measureTime(time_before_evocube, time_after_evocube);
    double time_post_evo = measureTime(time_after_evocube, time_after_post);
    coloredPrint("Evocube time: " + std::to_string(time_evocube), "cyan");

    std::string save_path = argv[2];
    Eigen::VectorXi save_labeling = final_indiv->getLabeling();

    std::cout << "Saving to:" << save_path << std::endl;
    saveFlagging(save_path + "/labeling.txt", save_labeling);
    // saveFlaggingOnTets(save_path + "/labeling_on_tets.txt", save_path + "/tris_to_tets.txt", save_labeling);

    saveFlagging(save_path + "/labeling_init.txt", labeling_init);
    igl::writePLY(save_path + "/fast_polycube_surf.ply", def_V, F, igl::FileEncoding::Ascii);
    igl::writePLY(save_path + "/init_surf.ply", V, F, igl::FileEncoding::Ascii);

    std::string logs_path = save_path + "/logs.json";
    final_indiv->fillIndivLogInfo(logs_path, "LabelingFinal");
    evaluator.fillIndivLogInfo(logs_path, *final_indiv, "LabelingFinal");

    std::shared_ptr<LabelingIndividual> graph_cut_indiv = std::make_shared<LabelingIndividual>(LabelingIndividual(evo, qle, labeling_init));
    graph_cut_indiv->updateChartsAndTPs(true);
    graph_cut_indiv->fillIndivLogInfo(logs_path, "LabelingGraphCut");
    evaluator.fillIndivLogInfo(logs_path, *graph_cut_indiv, "LabelingGraphCut");

    Eigen::VectorXi normal_labeling = normalFlagging(V, F);
    std::shared_ptr<LabelingIndividual> normal_indiv = std::make_shared<LabelingIndividual>(LabelingIndividual(evo, qle, normal_labeling));
    normal_indiv->updateChartsAndTPs(true);
    normal_indiv->fillIndivLogInfo(logs_path, "LabelingNormal");
    evaluator.fillIndivLogInfo(logs_path, *normal_indiv, "LabelingNormal");

    auto start_time = std::chrono::system_clock::now();
    std::time_t start_timet = std::chrono::system_clock::to_time_t(start_time);
    fillLogInfo("FinishTime", logs_path, std::ctime(&start_timet));

    // ALL THESE ARE IN CPU TIME! Since it's //, the sum is > to time_evocube
    fillLogInfo("Timing", "CreateIndiv", logs_path, sum_time_create_indiv);
    fillLogInfo("Timing", "Cross", logs_path, sum_time_cross);
    fillLogInfo("Timing", "Mutations", logs_path, sum_time_mut);
    fillLogInfo("Timing", "ChartsAndTps", logs_path, sum_time_chart);
    fillLogInfo("Timing", "Archive", logs_path, sum_time_archive);
    fillLogInfo("Timing", "Eval", logs_path, sum_time_eval);

    // Real-world time
    fillLogInfo("Timing", "PreGenetics", logs_path, time_init_evo);
    fillLogInfo("Timing", "Genetics", logs_path, time_evocube);
    fillLogInfo("Timing", "PostGenetics", logs_path, time_post_evo);

    fillLogInfo("#generations", logs_path, std::to_string(n_generations));
    fillLogInfo("#mutations_per_gen", logs_path, std::to_string(max_mut));

    fillLogInfo("GraphCutParams", "CompactCoeff", logs_path, compact_coeff);
    fillLogInfo("GraphCutParams", "FidelityCoeff", logs_path, fidelity_coeff);
    evo->fillMeshLogInfo(logs_path); 
    
    return 0;
}
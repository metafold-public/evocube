import sys
import os
import math

def to_lce_label(label):
    if label == 0: return 0
    if label == 1: return 0
    if label == 2: return 1
    if label == 3: return 1
    if label == 4: return 2
    if label == 5: return 2
    return -1

def main():

    # input 
    ply_path_in = sys.argv[1]
    labels_path_in = sys.argv[2]
    ply_path_out = sys.argv[3]

    with open(ply_path_in, 'r') as f:
        ply_data_in = f.readlines()

    with open(labels_path_in, 'r') as f:
        label_data_in = f.readlines()
    
    print(ply_data_in[2].split())
    # Get the active mesh (assumes we have a mesh object selected)
    num_verts = int(ply_data_in[2].split()[2])
    num_faces = int(ply_data_in[6].split()[2])

    with open(ply_path_out, 'w') as f:
        f.write('ply\n')
        f.write("format ascii 1.0\n")
        f.write("comment Created by Metafold3D\n")

        f.write(f"element vertex {num_verts}\n")
        f.write("property float x\n")
        f.write("property float y\n")
        f.write("property float z\n")

        f.write(f"element face {num_faces}\n")
        f.write("property list uchar uint vertex_indices\n")
        f.write("property uchar material_index\n")

        f.write("end_header\n")

        for i in range(0, num_verts):
            f.write(f"{ply_data_in[9 + i]}")
        
        for i in range(0, num_faces):
            label = to_lce_label(int(label_data_in[i]))
            f.write(f"{ply_data_in[9 + num_verts + i].strip()} {label}\n")

if __name__ == "__main__":
    main()